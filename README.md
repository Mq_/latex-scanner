# Latex Scanner

Scan Latex documents for patterns you want to avoid.
For example, common typos, enforce tilde before citations, find duplicate words, and more.

Link to the [Repository].

## Dependencies

See [requirements.txt](requirements.txt).

## Installing

Make sure `latex-scanner.py` is in your `$PATH`.

## Configuration

Create a file named `.latex-scanner.yml` in the directory of the Latex document you want to check.
List all files to scan in `files` and all patterns to search for in `patterns`.

[example.latex-scanner.yml](example.latex-scanner.yml) provides a template for the file with a few useful patterns.

Example patterns include:

- A pattern to find duplicate words.
- Patterns to find superfluous whitespace.
- Some patterns for good practices in LaTeX.

Specific matches of a pattern can be ignored by extending the defining pattern in the config file to an associative array with the entries `pattern` and `ignore`.
`ignore` is a list of patterns that cause a line to be ignored if they match the line.
An example can be found in the example config.

## Todo

[Repository]: https://gitlab.com/mq89/latex-scanner
