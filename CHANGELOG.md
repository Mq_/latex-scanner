# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.4.0] - 2024-02-14

### Added

- Catch YAML parsing error.
- Learn to create config file from template.
- Learn to present description for each rule.

### Changed

- Update Python version to 3.8.16.

## [1.3.0] - 2020-08-20

### Changed

- Remove ignore patterns from line before pattern check.

## [1.2.0] - 2020-08-17

### Fixed

- Fix test stage.

## [1.2.0-rc1] - 2020-08-17

### Added

- Catch FileNotFoundError.
- Add experimental support to find texfiles automatically.
- Add test stage to pipeline.

## [1.1.0] - 2020-08-11

### Added

- Print pattern in output.
- Use grep-style colors.
- Add Dockerfile and build for Docker Hub.

### Fixed

- Fix requirements.txt.

## [1.0.0] - 2020-06-16

### Added

- Allow pattern to be a dict in the config file to support patterns that can be selectively ignored.
- Add example configuration file.

### Fixed

- Fix catching empty configuration file.
- Fix off-by-one error in linecount.

## [0.1.0] - 2020-05-28

### Added

- Create initial version that compiles patterns and scans listed files.

[Unreleased]: https://gitlab.com/Mq_/latex-scanner/compare/v1.4.0...master
[1.4.0]: https://gitlab.com/Mq_/latex-scanner/compare/v1.3.0...v1.4.0
[1.3.0]: https://gitlab.com/Mq_/latex-scanner/compare/v1.2.0...v1.3.0
[1.2.0]: https://gitlab.com/Mq_/latex-scanner/compare/v1.2.0-rc1...v1.2.0
[1.2.0-rc1]: https://gitlab.com/Mq_/latex-scanner/compare/v1.1.0...v1.2.0-rc1
[1.1.0]: https://gitlab.com/Mq_/latex-scanner/compare/v1.0.0...v1.1.0
[1.0.0]: https://gitlab.com/Mq_/latex-scanner/compare/v0.1.0...v1.0.0
[0.1.0]: https://gitlab.com/Mq_/latex-scanner/tags/v0.1.0
