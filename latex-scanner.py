#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import os
import pprint
import re
import sys
from texutils import texutils
import yaml
import shutil

# version string
VERSION = "1.4.0"


def main():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-c", "--config", default=".latex-scanner.yml", help="Config file to use.")
    parser.add_argument("-v", "--verbose", action="store_true", help="Be more verbose.")
    parser.add_argument("-V", "--version", action="store_true", help="Show version information and exit.")
    parser.add_argument("-i", "--init", action="store_true", help="Copy template config file to working directory.")
    args = parser.parse_args()

    # print version and exit
    if args.version:
        print("latex-scanner v{}".format(VERSION))
        sys.exit(0)

    # copy template config and exit
    if args.init:
        template = os.path.dirname(os.path.realpath(__file__)) + "/example.latex-scanner.yml"
        if args.verbose:
            print("Template: {}".format(template))
        if not os.path.exists(template):
            print("Can not find {}.".format(template))
            sys.exit(1)
        target = args.config
        if os.path.exists(target):
            print("Target {} already exists.".format(target))
            sys.exit(1)
        shutil.copyfile(template, target)
        sys.exit(0)

    # load config
    config = {}
    if os.path.exists(args.config):
        with open(args.config) as f:
            try:
                config = yaml.load(f, Loader=yaml.Loader)
            except yaml.parser.ParserError as e:
                print(e)
                sys.exit(1)
    else:
        print("Can not find \"{}\".".format(args.config))
        sys.exit(1)

    # check for empty configuration
    if not config:
        print("No configuration to read.")
        sys.exit(1)

    if args.verbose:
        pprint.pprint(config)
        print()

    # check "files" configuration exists
    if "files" not in config:
        print("Can not find any files to scan in config.")
        sys.exit(1)

    # check "patterns" configuration exists
    if "patterns" not in config:
        print("Can not find any pattern to scan.")
        sys.exit(1)

    # compile configured patterns
    patterns = []
    for pattern in config["patterns"]:
        patterns.append(construct_pattern(pattern))

    if "texroot" in config:
        files = texutils.texfiles(config["texroot"])
    else:
        files = config["files"]

    # go through all files and search for all patterns
    c = 0
    for file in files:
        try:
            with open(file) as f:
                for i, line in enumerate(f.readlines()):
                    # ignore lines that are comments
                    if line[0] != "%":
                        for pattern in patterns:
                            output = f"\n\033[35m{file}:\033[32m{i + 1}\033[33m '{pattern.pattern}'\033[37m {pattern.description}\033[0m\n"
                            if pattern.search(line):
                                print("{}{}".format(
                                    output,
                                    pattern.sub(r'\033[31m\g<0>\033[0m', line.strip())))
                                c += 1
                            if pattern.line_was_ignored:
                                if args.verbose:
                                    print("{}\033[31mIGNORE:\033[0m\t{}".format(output, line.strip()))
        except FileNotFoundError:
            print("\033[31mCan not find file \"{}\".\033[0m".format(file))

    print("\nFound {} violation{}.".format(c, "" if c == 1 else "s"))

    if c > 0:
        sys.exit(1)


def construct_pattern(pattern):
    """Construct a new pattern object.

    Parameters
    ----------
    pattern : str or dict
        Serialization to construct the object from.

    Returns
    -------
    Pattern
        A new pattern object.

    Raises
    ------
    ValueError
        If type of argument does not match or does not contain the correct entries.
    """
    if type(pattern) is str:
        return Pattern(pattern, [], "")
    elif type(pattern) is dict:
        if "pattern" not in pattern:
            raise ValueError("Pattern does not provide pattern and ignore.")
        
        ignore = pattern["ignore"] if "ignore" in pattern else []
        description = pattern["description"] if "description" in pattern else ""

        return Pattern(pattern["pattern"], ignore, description)
    
    else:
        raise ValueError("Can not process argument type: {}".format(type(pattern)))


class Pattern:

    def __init__(self, pattern, ignore, description):
        """
        Create a new pattern object.
        
        Parameters
        ----------
        pattern : str
            Regualar expression.
        ignore : list
            List of strings containting regular expressions to be ignored by this pattern.
        
        :param      pattern:      The pattern
        :type       pattern:      str
        :param      ignore:       List of regex to be ignored
        :type       ignore:       list[str]
        :param      description:  The description
        :type       description:  str
        """
        self._pattern = re.compile(pattern)
        self._ignore = []
        for regex in ignore:
            self._ignore.append(re.compile(regex))
        self._description = description

    def search(self, line):
        """Search the line for this pattern after applying the ignore patterns.

        Parameters
        ----------
        line : str
            The line to search.

        Returns
        -------
        bool
            Whether the pattern was found in the line.
        """

        self._line_was_ignored = False

        before = self._pattern.search(line) is not None

        if not before:
            return False

        for pattern in self._ignore:
            line = re.sub(pattern, '', line)

        after = self._pattern.search(line) is not None

        self._line_was_ignored = before and not after

        return after

    def sub(self, repl, string):
        """Like re.sub()
        """
        return self._pattern.sub(repl, string)

    @property
    def pattern(self):
        return self._pattern.pattern

    @property
    def line_was_ignored(self):
        return self._line_was_ignored

    @property
    def description(self):
        return self._description


if __name__ == '__main__':
    main()
